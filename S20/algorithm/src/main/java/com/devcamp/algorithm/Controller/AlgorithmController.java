package com.devcamp.algorithm.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.algorithm.Service.AlgorithmService;

@RestController
@CrossOrigin
@RequestMapping("/api/string")
public class AlgorithmController {
  @Autowired
  private AlgorithmService algorithmService;

  @GetMapping("/reverse/{string}")
  public String convertReverse(@PathVariable String string) {
    return algorithmService.convertReverse(string);
  }

  @GetMapping("/check-reverse/{string}")
  public String checkReverse(@PathVariable String string) {
    return algorithmService.checkReverse(string);
  }

  @GetMapping("/remove-duplicates/{string}")
  public String removeDuplicates(@PathVariable String string) {
    return algorithmService.removeDuplicates(string);
  }

  @GetMapping("/concat-strings/{str1}/{str2}")
  public String concatStrings(@PathVariable String str1, @PathVariable String str2) {
    return algorithmService.concatStrings(str1, str2);
  }

}
