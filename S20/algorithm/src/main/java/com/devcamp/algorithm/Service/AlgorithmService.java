package com.devcamp.algorithm.Service;

import org.springframework.stereotype.Service;

@Service
public class AlgorithmService {
  public String convertReverse(String string) {
    String stringResult = "";
    StringBuilder stringInput = new StringBuilder(string);
    stringInput.reverse();
    stringResult = stringInput.toString();
    return stringResult;
  }

  public String checkReverse(String str) {
    String result = "Chuỗi đã nhập vào khớp chuỗi đảo ngược.";
    int n = str.length();
    for (int i = 0; i < n / 2; i++) {
      if (str.charAt(i) != str.charAt(n - i - 1)) {
        result = "Chuỗi đã nhập vào không khớp với chuỗi đảo ngược.";
        return result;
      }
    }
    return result;
  }

  public String removeDuplicates(String str) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < str.length(); i++) {
      char c = str.charAt(i);
      if (result.indexOf(String.valueOf(c)) == -1) {
        result.append(c);
      }
    }
    return result.toString();
  }

  public String concatStrings(String str1, String str2) {
    while (str1.length() != str2.length()) {
      if (str1.length() > str2.length()) {
        str1 = str1.substring(1);
      } else {
        str2 = str2.substring(1);
      }
    }
    return str1 + str2;
  }

}
