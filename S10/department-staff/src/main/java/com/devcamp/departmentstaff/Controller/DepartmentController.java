package com.devcamp.departmentstaff.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.departmentstaff.models.Department;
import com.devcamp.departmentstaff.services.DepartmentService;

@RestController
@CrossOrigin
@RequestMapping("/api/department")
public class DepartmentController {
  @Autowired
  private DepartmentService departmentService;

  @GetMapping("/list")
  public ArrayList<Department> getListDepartment() {
    return departmentService.getAllDepartment();
  }

  @GetMapping("/element-list/{id}")
  public Department getDepartmentById(@PathVariable int id) {
    return departmentService.getDepartmentById(id);
  }

  @GetMapping("/average-age/{index}")
  public ArrayList<Department> getAverageAgeOfDepartment(@PathVariable int index) {
    return departmentService.getAverageAgeOfDepartment(index);
  }
}
