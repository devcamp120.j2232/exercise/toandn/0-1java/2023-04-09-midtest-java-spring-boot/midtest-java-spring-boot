package com.devcamp.departmentstaff.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.departmentstaff.models.Staff;

@Service
public class StaffService {
  Staff staff1 = new Staff(1, "Nguyen Van A", 40);
  Staff staff2 = new Staff(2, "Nguyen Chi B", 38);
  Staff staff3 = new Staff(3, "Pham Chi C", 35);
  Staff staff4 = new Staff(4, "Lai Nhu D", 22);
  Staff staff5 = new Staff(5, "Nguyen Quoc E", 27);
  Staff staff6 = new Staff(6, "Thai Chien F", 32);
  Staff staff7 = new Staff(7, "Ly Dong G", 24);
  Staff staff8 = new Staff(8, "Dang Hoai H", 29);
  Staff staff9 = new Staff(9, "Lai Nhu D", 25);

  ArrayList<Staff> staffs = new ArrayList<>();

  public ArrayList<Staff> getStaffs() {
    return staffs;
  }

  public ArrayList<Staff> listStaffGroup1() {
    ArrayList<Staff> listStaff1 = new ArrayList<>();
    listStaff1.add(staff1);
    listStaff1.add(staff2);
    listStaff1.add(staff3);

    return listStaff1;
  }

  public ArrayList<Staff> listStaffGroup2() {
    ArrayList<Staff> listStaff2 = new ArrayList<>();
    listStaff2.add(staff4);
    listStaff2.add(staff5);
    listStaff2.add(staff6);

    return listStaff2;
  }

  public ArrayList<Staff> listStaffGroup3() {
    ArrayList<Staff> listStaff3 = new ArrayList<>();
    listStaff3.add(staff7);
    listStaff3.add(staff8);
    listStaff3.add(staff9);

    return listStaff3;
  }

  public ArrayList<Staff> getListStaffs() {
    ArrayList<Staff> allListstaff = new ArrayList<>();
    allListstaff.addAll(listStaffGroup1());
    allListstaff.addAll(listStaffGroup2());
    allListstaff.addAll(listStaffGroup3());
    return allListstaff;
  }

  public ArrayList<Staff> getStaffFromAge(int ageStaff) {
    ArrayList<Staff> allListstaff = new ArrayList<>();
    allListstaff.addAll(listStaffGroup1());
    allListstaff.addAll(listStaffGroup2());
    allListstaff.addAll(listStaffGroup3());

    ArrayList<Staff> listAgeStaff4 = new ArrayList<>();
    for (int i = 0; i < allListstaff.size(); i++) {
      if (allListstaff.get(i).getAge() > ageStaff) {
        listAgeStaff4.add(allListstaff.get(i));
      }
    }
    return listAgeStaff4;
  }
}
