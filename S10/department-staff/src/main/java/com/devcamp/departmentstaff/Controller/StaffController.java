package com.devcamp.departmentstaff.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.departmentstaff.models.Staff;
import com.devcamp.departmentstaff.services.StaffService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class StaffController {
  @Autowired
  private StaffService staffService;

  @GetMapping("/list-staff")
  public ArrayList<Staff> getListStaffs() {
    return staffService.getListStaffs();
  }

  @GetMapping("/age-staff")
  public ArrayList<Staff> getStaffFromAge(@RequestParam(name = "ageStaff") int ageStaff) {
    return staffService.getStaffFromAge(ageStaff);
  }

}
