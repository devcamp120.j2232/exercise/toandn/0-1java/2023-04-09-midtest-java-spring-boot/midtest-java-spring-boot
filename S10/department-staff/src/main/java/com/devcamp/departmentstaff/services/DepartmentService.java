package com.devcamp.departmentstaff.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.departmentstaff.models.Department;
import com.devcamp.departmentstaff.models.Staff;

@Service
public class DepartmentService {
  @Autowired
  private StaffService staffService;

  ArrayList<Department> departments = new ArrayList<>();

  public ArrayList<Department> getDepartments() {
    return departments;
  }

  public Department getDepartment1() {
    Department department1 = new Department(1, "IT", "floor 3, room 2",
        staffService.listStaffGroup1());
    return department1;
  }

  public Department getDepartment2() {
    Department department2 = new Department(2, "HR", "floor 3, room 5",
        staffService.listStaffGroup2());
    return department2;
  }

  public Department getDepartment3() {
    Department department3 = new Department(3, "R&D", "floor 1, room 3",
        staffService.listStaffGroup3());
    return department3;
  }

  public ArrayList<Department> getAllDepartment() {
    ArrayList<Department> listDepartment = new ArrayList<>();

    listDepartment.add(getDepartment1());
    listDepartment.add(getDepartment2());
    listDepartment.add(getDepartment3());

    return listDepartment;
  }

  public Department getDepartmentById(int id) {
    ArrayList<Department> department = new ArrayList<>();
    department.addAll(getAllDepartment());
    Department departmentResult = null;
    for (int i = 0; i < department.size(); i++) {
      if (department.get(i).getId() == id)
        departmentResult = department.get(i);
    }
    return departmentResult;
  }

  public ArrayList<Department> getAverageAgeOfDepartment(int averageAge) {
    ArrayList<Department> department = new ArrayList<>();
    department.addAll(getAllDepartment());
    ArrayList<Department> departmentResult = new ArrayList<>();
    for (int i = 0; i < department.size(); i++) {
      if (department.get(i).getAverageAge() > averageAge)
        departmentResult.add(department.get(i));
    }
    return departmentResult;
  }

}
