package com.devcamp.departmentstaff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DepartmentStaffApplication {

	public static void main(String[] args) {
		SpringApplication.run(DepartmentStaffApplication.class, args);
	}

}
